package com.pdwsn.ui;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;

import io.vov.vitamio.utils.Log;
import io.vov.vitamio.widget.VideoView;

import com.example.envactivity.R;
import com.pdwsn.Bean.JSONUtil;
import com.pdwsn.Bean.VideosInfoBean;
import com.pdwsn.videos.PlayViedosActivity;
import com.pdwsn.videos.VideosListAdapter;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;


public class VideosFragment extends Fragment {
	private ListView mListView;
	private ProgressBar videoProgress;
	private TextView videoNoFind;
	private final int GET_OK = 0x02;
	private final int GET_FAILED = 0x03;
	private List<VideosInfoBean> mVList = null;
	private final String get_img_list = "get_img_list";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.videos_fragment, container, false);
	}

	private Timer tr;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		mListView = (ListView) getActivity().findViewById(R.id.videos_listview);
		videoProgress = (ProgressBar)getActivity().findViewById(R.id.video_progressBar);
		videoNoFind = (TextView)getActivity().findViewById(R.id.video_no_find);
		tr = new Timer();
		tr.schedule(new TimerTask() {
			final Handler handler = new Handler() {

				@Override
				public void handleMessage(Message msg) {
					if (msg.what == GET_OK) {
						tr.cancel();
						videoNoFind.setVisibility(View.GONE);
						videoProgress.setVisibility(View.GONE);
						mListView.setVisibility(View.VISIBLE);
						mListView.setAdapter(new VideosListAdapter(
								getActivity(), mVList));
						mListView.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> parent,
									View view, int position, long id) {
								Log.d("position", String.valueOf(position));
								// TODO Auto-generated method stub
								Bundle data = new Bundle();
								data.putInt("position", position);
								Intent intent = new Intent(getActivity(), PlayViedosActivity.class);
								intent.putExtra("data", data);
								
								startActivity(intent);
								//getActivity().fileList();
							}
						});
					}else if(msg.what == GET_FAILED){
						tr.cancel();
						videoProgress.setVisibility(View.GONE);
						mListView.setVisibility(View.GONE);
						videoNoFind.setVisibility(View.VISIBLE);
					}
				}
			};

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					mVList = JSONUtil.getDataList(MainFragment.BASE_URL,
							get_img_list);
					if (mVList != null) {
						Message msg = new Message();
						msg.what = GET_OK;
						handler.sendMessage(msg);
					}else{
						Message msg = new Message();
						msg.what = GET_FAILED;
						handler.sendMessage(msg);
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}, 0, 1000);
	}	
}
