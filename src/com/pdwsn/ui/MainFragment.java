package com.pdwsn.ui;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import com.dacer.androidcharts.LineView;
import com.example.envactivity.R;
import com.pdwsn.Bean.CurBean;
import com.pdwsn.Bean.JSONUtil;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.FrameLayout.LayoutParams;

public class MainFragment extends Fragment {
	/**
	 * 访问的后台地址，这里访问本地的不能用127.0.0.1应该用10.0.2.2
	 */
	public static final String BASE_URL = "http://byh.pdwsn.com/api/get_data.php";

	private final String get_cur = "get_cur";
	private final String get_sta = "get_sta";
	private CurBean curBean;
	private final int GET_OK = 0x11;
	private Timer tr;
	int randomint = 9;
	private LineView lineView;
	private GridView gridView=null;  
	private MyDataAdapter myadapter;
	private String[] data = {"20.0", "20.1", "20.2", "20.3"};
	private String[] name = {"空气温度", "空气湿度", "土壤温度", "土壤湿度"};
	private String[] flag = {"℃", "%","℃","%"};
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.main_fragment, container,
				false);
		gridView=(GridView) rootView.findViewById(R.id.grid_view);  
		gridView.setSelection(1);
		gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
		
		lineView = (LineView) rootView.findViewById(R.id.line_view);
		showTopData(data, name, flag);
		return rootView;
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		data = new String[4];
//		data[0] = "123";
//		data[1] = "323";
//		data[2] = "533";
//		data[3] = "233";
//		myadapter.setData(data);
//		myadapter.notifyDataSetChanged();
		Format f = new SimpleDateFormat("MM-dd");
		Calendar c = Calendar.getInstance();
		
		// must*
		ArrayList<String> DateList = new ArrayList<String>();
		for (int i = 0; i < randomint; i++) {
			c = getADate(c, -1);
			DateList.add(f.format(c.getTime()));
		}
		Collections.reverse(DateList);
		lineView.setBottomTextList(DateList);
		lineView.setDrawDotLine(true);
		lineView.setShowPopup(LineView.SHOW_POPUPS_All);
		randomSet(lineView);
		tr = new Timer();
		tr.schedule(new TimerTask() {
			final Handler handler = new Handler() {

				@Override
				public void handleMessage(Message msg) {
					if (msg.what == GET_OK) {
						tr.cancel();
						data = new String[4];
						data[0] = getFrontFourData(curBean.getATemp());
						data[1] = getFrontFourData(curBean.getAHumi());
						data[2] = getFrontFourData(curBean.getSTemp());
						data[3] = getFrontFourData(curBean.getSHumi());						
						myadapter.setData(data);
						myadapter.notifyDataSetChanged();
						
						Log.d("curbean",
								curBean.getATemp() + curBean.getSTemp()
										+ curBean.getAHumi()
										+ curBean.getSHumi());
					}
				}
			};

			@Override
			public void run() {
				// TODO Auto-generated method stub

				try {
					Thread.sleep(1000);
					JSONObject obj = JSONUtil.getData(BASE_URL, get_cur);
					curBean = writeCurBean(obj);
					Message msg = new Message();
					msg.what = GET_OK;
					handler.sendMessage(msg);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}, 0, 1000);
	}

	// 获取前后几天的时间
	private static Calendar getADate(Calendar c, int day) {
		c.add(Calendar.DATE, day);
		return c;
	}

	private void randomSet(LineView lineView) {
		ArrayList<Integer> dataList = new ArrayList<Integer>();
		int random = (int) (Math.random() * 9 + 21);
		for (int i = 0; i < randomint; i++) {
			dataList.add((int) (Math.random() * random));
		}

		ArrayList<Integer> dataList2 = new ArrayList<Integer>();
		random = (int) (Math.random() * 9 + 21);
		for (int i = 0; i < randomint; i++) {
			dataList2.add((int) (Math.random() * random));
		}

		ArrayList<Integer> dataList3 = new ArrayList<Integer>();
		random = (int) (Math.random() * 9 + 21);
		for (int i = 0; i < randomint; i++) {
			dataList3.add((int) (Math.random() * random));
		}

		ArrayList<ArrayList<Integer>> dataLists = new ArrayList<ArrayList<Integer>>();
		dataLists.add(dataList);
		dataLists.add(dataList2);
		// dataLists.add(dataList3);
		lineView.setDataList(dataLists);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	}

	public void showTopData(String[] data, String[] name, String[] flag) {		
		myadapter=new MyDataAdapter(getActivity(), data, flag, name); 
		// 初始化
		myadapter.setSeclection(0);
		myadapter.notifyDataSetChanged();
		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				myadapter.setSeclection(position);
				myadapter.notifyDataSetChanged();
				randomSet(lineView);
			}
		});
		gridView.setAdapter(myadapter);
	}

	/**
	 * 取字符串前4位
	 */
	public String getFrontFourData(String str) {
		if (str.length() >= 4) {
			return str.substring(0, 4);
		}
		if (str.length() == 2) {
			return str + ".0";
		}
		return str;
	}

	/**
	 * 将获取的Cur 数据写入 CurBean里
	 * 
	 * @param obj
	 * @return
	 * @throws JSONException
	 */
	private CurBean writeCurBean(JSONObject obj) throws JSONException {
		CurBean curBean = new CurBean();
		curBean.setAHumi(obj.getString("air_humi"));
		curBean.setATemp(obj.getString("air_temp"));
		curBean.setSTemp(obj.getString("soil_temp"));
		curBean.setSHumi(obj.getString("soil_humi"));
		curBean.setLight(obj.getString("light"));
		curBean.setId(obj.getString("id"));
		curBean.setDid(obj.getString("dev_id"));
		curBean.setUtime(obj.getString("upd_time"));
		return curBean;
	}
}
