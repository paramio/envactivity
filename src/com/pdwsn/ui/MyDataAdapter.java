package com.pdwsn.ui;

import com.example.envactivity.R;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class MyDataAdapter extends BaseAdapter {
	private String[] data = null;
	private String[] flag = null;
	private String[] name = null;
	private Context context = null;
	private LayoutInflater inflater = null;
	private int clickTemp = -1;

	public MyDataAdapter(Context context, String[] data, String[] flag,
			String[] name) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.flag = flag;
		this.name = name;
		this.data = data;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	private class Holder {

		DataView dataView = null;

		public DataView getDv() {
			return dataView;
		}

		public void setTv(DataView dataView) {
			this.dataView = dataView;
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.gridview_item, null);
			holder = new Holder();
			holder.dataView = (DataView) convertView.findViewById(R.id.mydv);
			convertView.setTag(holder);

		} else {
			holder = (Holder) convertView.getTag();

		}
		holder.dataView.setName(name[position]);
		holder.dataView.setFlag(flag[position]);
		holder.dataView.setData(data[position]);

		if (clickTemp == position) {
			holder.dataView.setBackgroundColor(Color.LTGRAY);
		} else {
			holder.dataView.setBackgroundColor(Color.TRANSPARENT);
		}
		return convertView;
	}

	public void setSeclection(int position) {
		clickTemp = position;
	}
	public void setData(String[] data){
		this.data = data;
	}
}
