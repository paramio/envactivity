package com.pdwsn.ui;

import java.util.Timer;
import java.util.TimerTask;

import com.example.envactivity.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;

public class AboutUsActivity extends Activity {
    private Timer timer = new Timer();  
    private TimerTask task;  
    private final Handler handler = new Handler() {  
        @Override  
        public void handleMessage(Message msg) {  
            // TODO Auto-generated method stub  
            super.handleMessage(msg);
            Intent intent = new Intent(AboutUsActivity.this, MainActivity.class);
			startActivity(intent);
            timer.cancel();  
        }  
    };  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_us);	
		
		Button enterBtn = (Button)findViewById(R.id.enter);
		
		enterBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AboutUsActivity.this, MainActivity.class);
				startActivity(intent);
				timer.cancel();
			}
		});
		
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		timer = new Timer();  
		task = new TimerTask() {  
		    @Override  
		    public void run() {  
		        // TODO Auto-generated method stub  
		        Message message = new Message();  
		        message.what = 1;  
		        handler.sendMessage(message);  
		    }  
		}; 
		timer.schedule(task, 5000);
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		timer.cancel();
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		timer.cancel();
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		timer.cancel();
	}


}
