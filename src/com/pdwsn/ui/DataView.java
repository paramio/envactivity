package com.pdwsn.ui;

import java.io.InputStream;

import com.example.envactivity.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class DataView extends View{
	private String data;
	private String flag;
	private String name;
	private int bgcolor;
	private int textcolor;
	private Paint mPaint;
	private Rect mBounds;
	private Bitmap thermoPic;
	public DataView(Context context){
		super(context);
		mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mBounds = new Rect();
		InputStream in = getResources().openRawResource(R.drawable.pic1);
		thermoPic = BitmapFactory.decodeStream(in);
		this.bgcolor = Color.BLACK;
		this.textcolor = Color.BLUE;
		this.data = "00.0";
		this.flag = "%";
		this.name = "空气温度";
		
	}
	
	public DataView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mBounds = new Rect();
		InputStream in = getResources().openRawResource(R.drawable.pic1);
		thermoPic = BitmapFactory.decodeStream(in);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.DataView);
		try {
			this.data = a.getString(R.styleable.DataView_data);
			this.flag = a.getString(R.styleable.DataView_flag);
			this.name = a.getString(R.styleable.DataView_name);
			this.bgcolor = a
					.getColor(R.styleable.DataView_bgcolor, Color.BLACK);
			this.textcolor = a.getColor(R.styleable.DataView_textcolor,
					Color.WHITE);
			Log.d("thisismydata",data);
		} finally {
			a.recycle();
		}
	}
	
	

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		mPaint.setColor(bgcolor);
		mPaint.setAlpha(130);
		canvas.drawRect(0, 0, getWidth(), getHeight(), mPaint);
		mPaint.setColor(textcolor);
		mPaint.setTextSize(60);
		String text = data;
		canvas.drawText(text, 10, getHeight() - 30, mPaint);
		mPaint.getTextBounds(flag, 0, flag.length(), mBounds);
		mPaint.setTextSize(60);
		canvas.drawText(flag, getWidth() - mBounds.width() / 2 - 40,
				getHeight() - 40, mPaint);
		mPaint.getTextBounds(name, 0, name.length(), mBounds);
		mPaint.setTextSize(32);
		canvas.drawText(name, getWidth() - mBounds.width() / 2 - 20,
				mBounds.height() / 2 + 10, mPaint);
		canvas.drawBitmap(thermoPic, 10, 10, mPaint);

	}

	/**
	 * 设置传入的数据
	 * 
	 * @param data
	 */
	public synchronized void setData(String data) {
		this.data = data;
		// 非线程的的刷新界面
		postInvalidate();
	}
	
	public synchronized void setName(String name) {
		this.name = name;
		// 非线程的的刷新界面
		postInvalidate();
	}

	public synchronized void setFlag(String flag) {
		this.flag = flag;
		// 非线程的的刷新界面
		postInvalidate();
	}

}
