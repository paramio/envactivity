package com.pdwsn.videos;

import java.io.IOException;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.List;
import java.util.zip.Inflater;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;

import com.example.envactivity.R;
import com.pdwsn.Bean.Global;
import com.pdwsn.Bean.VideosInfoBean;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class VideosListAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater listContainer;
	private List<VideosInfoBean> videos;

	public final class ListItemView {
		private ImageView imageView;
		private TextView nameTextView;
		private TextView ipTextView;
		private TextView channelTextView;
	}

	public VideosListAdapter(Context context, List<VideosInfoBean> videos) {
		this.context = context;		
		this.videos = videos;
		this.listContainer = LayoutInflater.from(this.context); // 创建视图容器并设置上下文
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return videos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return videos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	private ImageView miv;
	private TextView ntv; 
	private TextView ctv; 
	private TextView itv; 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		// 自定义视图
		ListItemView listItemView = null;
		if (convertView == null) {
			listItemView = new ListItemView();
			convertView = listContainer.inflate(R.layout.videos_list, null);
			listItemView.imageView = (ImageView) convertView
					.findViewById(R.id.videos_preview);
			listItemView.nameTextView = (TextView) convertView
					.findViewById(R.id.name_text_view);
			listItemView.ipTextView = (TextView) convertView
					.findViewById(R.id.ip_text_view);
			listItemView.channelTextView = (TextView) convertView
					.findViewById(R.id.channel_text_view);
			// 设置控件集到convertView
			convertView.setTag(listItemView);
		} else {
			// 取回控件
			listItemView = (ListItemView) convertView.getTag();
		}
		miv = listItemView.imageView;
		ntv = listItemView.nameTextView;
		ctv = listItemView.channelTextView;
		itv = listItemView.ipTextView;
		String http_url = ((VideosInfoBean)getItem(position)).getUrl();
		Log.d("bitmapurl", http_url);
		 new AsyncImageLoader().loadDrawable(http_url, position, new ImageCallback() {
			 
	            @SuppressLint("NewApi")
	            public void imageLoaded(Drawable imageDrawable, String imageUrl, int position) {
	                System.out.println("图片获取完成");
	                Log.d("drawable", String.valueOf(imageDrawable.getIntrinsicWidth()));
	                miv.setImageDrawable(imageDrawable);
	        		ntv.setText(((VideosInfoBean)getItem(position)).getName());
	        		ctv.setText(((VideosInfoBean)getItem(position)).getChannel());
	        		itv.setText(((VideosInfoBean)getItem(position)).getIp());
	                 
	            }
	        });
		
		return convertView;
	}
	// 解析服务器图片
	public Bitmap getBitmap(String http_url)  
    {  
        Bitmap bitmap = null;  
        try  
        {  
        	Log.d("bitmapurl", http_url);
            URL url = new URL(http_url);  
            bitmap = BitmapFactory.decodeStream(url.openStream());  
        } catch (Exception e)  
        {  
            // TODO Auto-generated catch block  
            e.printStackTrace();  
        }  
          
        return bitmap;  
    }  
	/**
	 * 定义回调接口
	 */
	public interface ImageCallback {
		public void imageLoaded(Drawable imageDrawable, String imageUrl, int position);
	}

	/**
	 * 异步加载图片
	 */
	static class AsyncImageLoader {

		Global global;

		public AsyncImageLoader() {
			global = Global.getInstance();
		}

		/**
		 * 创建子线程加载图片 子线程加载完图片交给handler处理（子线程不能更新ui，而handler处在主线程，可以更新ui）
		 * handler又交给imageCallback，imageCallback须要自己来实现，在这里可以对回调参数进行处理
		 *
		 * @param imageUrl
		 *            ：须要加载的图片url
		 * @param imageCallback
		 *            ：
		 * @return
		 */
		public Drawable loadDrawable(final String imageUrl, final int position,
				final ImageCallback imageCallback) {
			Log.d("imageurl", imageUrl);
			// 如果缓存中存在图片 ，则首先使用缓存
			if (global.getCache(imageUrl) != null) {
				System.out.println("存在缓存~~~~~~~~~~~~~~~~~");
				SoftReference<Drawable> softReference = global
						.getCache(imageUrl);
				Drawable drawable = softReference.get();
				if (drawable != null) {
					imageCallback.imageLoaded(drawable, imageUrl, position);// 执行回调
					return drawable;
				}
			}

			/**
			 * 在主线程里执行回调，更新视图
			 */
			final Handler handler = new Handler() {
				public void handleMessage(Message message) {
					imageCallback.imageLoaded((Drawable) message.obj, imageUrl, position);
				}
			};

			/**
			 * 创建子线程访问网络并加载图片 ，把结果交给handler处理
			 */
			new Thread() {
				@Override
				public void run() {
					
					Drawable drawable = loadImageFromUrl(imageUrl);
					// 下载完的图片放到缓存里
					global.setCache(imageUrl, new SoftReference<Drawable>(
							drawable));
					Message message = handler.obtainMessage(0, drawable);
					handler.sendMessage(message);
				}
			}.start();

			return null;
		}

		/**
		 * 下载图片 （注意HttpClient 和httpUrlConnection的区别）
		 */
		public Drawable loadImageFromUrl(String url) {

			try {
				HttpClient client = new DefaultHttpClient();
				client.getParams().setParameter(
						CoreConnectionPNames.CONNECTION_TIMEOUT, 1000 * 15);
				HttpGet get = new HttpGet(url);
				HttpResponse response;

				response = client.execute(get);
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					Log.d("HttpStatus", String.valueOf(HttpStatus.SC_OK));
					HttpEntity entity = response.getEntity();
					//Log.d("Httpcon", entity.getContent().toString());

					Drawable d = Drawable.createFromStream(entity.getContent(),
							"src");

					return d;
				} else {
					return null;
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}
	}
}
