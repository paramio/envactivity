package com.pdwsn.videos;

import com.example.envactivity.R;

import io.vov.vitamio.LibsChecker;
import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.utils.Log;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class PlayViedosActivity extends Activity {
	private String path = "";
	private VideoView mVideoView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		if (!LibsChecker.checkVitamioLibs(this))
			return;
		setContentView(com.example.envactivity.R.layout.play_video);
		Intent intent = getIntent();
		Bundle data = intent.getExtras();
		Log.d("myposition", String.valueOf(data.getInt("position")));
		switch(data.getInt("position")){
		case 0: path = "rtmp://218.2.74.158/byh/camera13";
		break;
		case 1: path = "rtmp://218.2.74.158/byh/camera9";
		break;
		case 2: path = "rtmp://218.2.74.158/byh/camera3";
		break;
		case 3: path = "rtmp://218.2.74.158/byh/camera1";
		break;
		case 4: path = "rtmp://218.2.74.158/byh/camera7";
		break;
		}
		mVideoView = (VideoView) findViewById(R.id.surface_view);
		if (path == "") {
			// Tell the user to provide a media file URL/path.
			Toast.makeText(PlayViedosActivity.this, "Please edit VideoViewDemo Activity, and set path" + " variable to your media file URL/path", Toast.LENGTH_LONG).show();
			return;
		} else {
			/*
			 * Alternatively,for streaming media you can use
			 * mVideoView.setVideoURI(Uri.parse(URLstring));
			 */
			mVideoView.setVideoPath(path);
			//mVideoView.setMediaController(new MediaController(this));
			mVideoView.requestFocus();
			
			mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mediaPlayer) {
					// optional need Vitamio 4.0
					mediaPlayer.setPlaybackSpeed(1.0f);
				}
			});
		}
		Button back = (Button) findViewById(R.id.title_bar_back);
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PlayViedosActivity.this.finish();
			}
		});
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mVideoView.resume();
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mVideoView.pause();
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		mVideoView.stopPlayback();
	}


}
