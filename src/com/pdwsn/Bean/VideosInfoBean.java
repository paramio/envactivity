package com.pdwsn.Bean;

public class VideosInfoBean {
	private String url;
	private String date;
	private String channel;
	private String ip;
	private String name;
	
	public VideosInfoBean(String url, String date, String channel, String ip, String name){
		this.url = url;
		this.date = date;
		this.channel = channel;
		this.ip = ip;
		this.name = name; 
	}
	public String getUrl(){
		return this.url;
	}
	public String getDate(){
		return this.date;
	}
	public String getIp(){
		return this.ip;
	}
	public String getChannel(){
		return this.channel;
	}
	public String getName(){
		return this.name;
	}
	
}
