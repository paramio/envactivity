package com.pdwsn.Bean;

public class EnvBean {
	private float temperatrue;
	private float humidity;
	private float averageTemp;
	private float averageHumi;
	
	public EnvBean(float temperatrue, float humidity, float averageTemp, float averageHumi){
		this.temperatrue = temperatrue;
		this.humidity = humidity;
		this.averageTemp = averageTemp;
		this.averageHumi = averageHumi;
	}
	public void setTemp(float temp){
		this.temperatrue = temp;
	}
	public void setHumi(float humi){
		this.humidity = humi;
	}
	public void setAveTemp(float aveTemp){
		this.averageTemp = aveTemp;
	}
	public void setAveHumi(float aveHumi){
		this.averageHumi = aveHumi;
	}
	public float getTemp(){
		return this.temperatrue;
	}
	public float getHumi(){
		return this.humidity;
	}
	public float getAveTemp(){
		return this.averageTemp;
	}
	public float getAveHumi(){
		return this.averageHumi;
	}
}
