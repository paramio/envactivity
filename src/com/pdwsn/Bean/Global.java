package com.pdwsn.Bean;

import java.lang.ref.SoftReference;
import java.util.HashMap;
 

import com.example.envactivity.R.drawable;

import android.graphics.drawable.Drawable;
 
/**
 * 全局变量，以软引用方式存放图片缓存
 *
 */
public class Global {
 
    // 软引用，使用内存做临时缓存 （程序退出，或内存不够则清除软引用）
    private static HashMap<String, SoftReference<Drawable>> imageCache;
 
    private static Global global;
 
    public static Global getInstance() {
 
        if (global == null) {
            global = new Global();
        }
 
        if (imageCache == null) {
            imageCache = new HashMap<String, SoftReference<Drawable>>();
        }
 
        return global;
 
    }
 
     
    //存放缓存
    public void setCache(String url, SoftReference<Drawable> softReference) {
 
        imageCache.put(url, softReference);
         
    }
 
    //获取缓存
    public SoftReference<Drawable> getCache(String url) {
         
        return imageCache.get(url);
         
    }
     
    //清除缓存
    public void clearCache() {
        if (imageCache.size() > 0) {
            imageCache.clear();
        }
    }
     
     
}
