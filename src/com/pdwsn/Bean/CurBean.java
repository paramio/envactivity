package com.pdwsn.Bean;

public class CurBean {
	private String id;	
	private String dev_id;	
	private String air_humi;
	private String air_temp;
	private String soil_humi;
	private String soil_temp;
	private String light;
	private String upd_time;
	
	public CurBean(){
	}
	public void setATemp(String air_temp){
		this.air_temp = air_temp;
	}
	public void setAHumi(String air_humi){
		this.air_humi = air_humi;
	}
	public void setSTemp(String soil_temp){
		this.soil_temp = soil_temp;
	}
	public void setSHumi(String soil_humi){
		this.soil_humi = soil_humi;
	}
	public void setId(String id){
		this.id = id;
	}
	public void setDid(String dev_id){
		this.dev_id = dev_id;
	}
	public void setUtime(String upd_time){
		this.upd_time = upd_time;
	}
	public void setLight(String light){
		this.light = light;
	}
	public String getATemp(){
		return this.air_temp;
	}
	public String getAHumi(){
		return this.air_humi;
	}
	public String getSTemp(){
		return this.soil_temp;
	}
	public String getSHumi(){
		return this.soil_humi;
	}
	public String getId(){
		return this.id;
	}
	public String getDid(){
		return this.dev_id;
	}
	public String getUtime(){
		return this.upd_time;
	}
	public String getLight(){
		return this.light;
	}
}
