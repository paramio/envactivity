package com.pdwsn.Bean;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * @author paramio Json封装的工具类.
 */
public class JSONUtil {

	private static final String TAG = "JSONUtil";

	/**
	 * 获取 接收到的 data 数据对象
	 * 
	 * @param url
	 * @param parameter
	 * @return
	 * @throws Exception
	 */
	public static JSONObject getData(String url, String parameter)
			throws Exception {
		// 发送 get请求 获取JSONobject
		JSONObject obj = getJSON(url, parameter);
		Log.d("air_temp", obj.toString());
		// 解析JSON对象 将data 读取data 的数组对象
		JSONArray jsonArr = obj.getJSONArray("data");
		// 读取 数组对象的第一 条数据 获取JSON对象
		obj = jsonArr.getJSONObject(0);

		return obj;
	}

	public static List<VideosInfoBean> getDataList(String url, String parameter)
			throws Exception {
		List<VideosInfoBean> VList = new ArrayList<VideosInfoBean>();
		// 发送 get请求 获取JSONobject
		JSONObject obj = getJSON(url, parameter);
		Log.d("air_temp", obj.toString());
		if (obj != null) {
			// 解析JSON对象 将data 读取data 的数组对象
			JSONArray jsonArr = obj.getJSONArray("data");
			// 读取 所有数组对象条数据 获取JSON对象
			for (int i = 0; i < jsonArr.length(); i++) {
				VList.add(new VideosInfoBean((jsonArr.getJSONObject(i))
						.getString("pic_thumb"), (jsonArr.getJSONObject(i))
						.getString("pic_time"), (jsonArr.getJSONObject(i))
						.getString("pic_channel"), (jsonArr.getJSONObject(i))
						.getString("pic_ip"), (jsonArr.getJSONObject(i))
						.getString("pic_name")));
			}

			return VList;
		}
		return null;
	}

	/**
	 * 获取json内容
	 * 
	 * @param url
	 * @return JSONArray
	 * @throws JSONException
	 * @throws ConnectionException
	 */
	public synchronized static JSONObject getJSON(String url, String parameter)
			throws JSONException, Exception {

		String url_para = url + "?data_bac=" + parameter;
		Log.d("air_temp", url_para);
		return new JSONObject(getRequest(url_para));
	}

	/**
	 * 向api发送get请求，返回从后台取得的信息。
	 * 
	 * @param url
	 * @param client
	 * @return String
	 */
	protected synchronized static String getRequest(String url)
			throws Exception {
		String result = null;
		Log.d(TAG, "do the getRequest,url=" + url + "");
		try {
			URL _url = new URL(url);
			HttpURLConnection conn;
			conn = (HttpURLConnection) _url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
			conn.connect();

			if (conn.getResponseCode() == 200) {
				Log.d("noget", "ok");
				InputStream in = conn.getInputStream();
				String raw = read(in);
				// 处理成正确的 JSON 字符串
				result = raw.substring(1, raw.length() - 1);
			} else {
				return "failed";
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			throw new Exception(e);
		}
		return result;
	}

	/**
	 * 读取字符流
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 */
	private static String read(InputStream in) throws IOException {
		byte[] data;
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int len = 0;
		while ((len = in.read(buf)) != -1) {
			bout.write(buf, 0, len);
		}
		data = bout.toByteArray();
		return new String(data, "UTF-8");
	}

	/**
	 * 处理httpResponse信息,返回String
	 * 
	 * @param httpEntity
	 * @return String
	 */
	protected static String retrieveInputStream(HttpEntity httpEntity) {

		int length = (int) httpEntity.getContentLength();
		// the number of bytes of the content, or a negative number if unknown.
		// If the content length is known but exceeds Long.MAX_VALUE, a negative
		// number is returned.
		// length==-1，下面这句报错，println needs a message
		if (length < 0)
			length = 10000;
		StringBuffer stringBuffer = new StringBuffer(length);
		try {
			InputStreamReader inputStreamReader = new InputStreamReader(
					httpEntity.getContent(), HTTP.UTF_8);
			char buffer[] = new char[length];
			int count;
			while ((count = inputStreamReader.read(buffer, 0, length - 1)) > 0) {
				stringBuffer.append(buffer, 0, count);
			}
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, e.getMessage());
		} catch (IllegalStateException e) {
			Log.e(TAG, e.getMessage());
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return stringBuffer.toString();
	}
}